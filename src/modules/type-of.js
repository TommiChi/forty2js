const typeOf = (target, type) => {
    let parsedType = Object.prototype.toString.call(target).split(' ')[1].replace(/[\[\]]/g, '').toLowerCase();

    return (type) ? parsedType === type : parsedType;
};

export default typeOf;