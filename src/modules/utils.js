import { promise as promise } from './promise';

const extend = (target = {}) => {
        return {
            'with':(extensionObject = {}, params = {}) => {

                params.deep = params.deep || false;
                params.clone = params.clone || false;

                let keys = Object.keys(extensionObject);
                let responseObject = (params.clone) ? clone(target) : target;

                /**
                 * I am using a for loop because I do not know how big the object I am extending is going to be,
                 * and you can squeeze marginally more performance out of a for loop
                 */
                for(let i = keys.length; i--;) {
                    if(responseObject[keys[i]] === null || responseObject[keys[i]] === undefined) {
                        responseObject[keys[i]] = extensionObject[keys[i]];
                    }
                    else if(typeOf(responseObject[keys[i]], 'object') && typeOf(extensionObject[keys[i]], 'object') && params.deep) {
                        extend(responseObject[keys[i]]).with(extensionObject[keys[i]], {deep:true, clone:false});
                    }
                }

                return responseObject;
            }
        };
    },

    cloneTypes = {
        ['array']:(target) => {
            return [].concat(target);
        },
        ['function']:(target) => {
            return new Function('return '+ target.toString())(target);
        },
        ['object']:(target) => {
            let newClone = {};

            Object.keys(target).map(function(key) {
                let objectType = typeOf(target[key]);

                if(['array', 'function', 'object'].indexOf(objectType)) {
                    newClone[key] = clone(target[key]);
                }
                else {
                    newClone[key] = target[key];
                }

            });

            return newClone;
        }
    },

    clone = (target) => {
        let returnObject,
            objectType = typeOf(target);

        if(['array', 'function', 'object'].indexOf(objectType) > -1) {
             returnObject = cloneTypes[objectType](target);
        }
        else {
            returnObject = target;
        }

        return returnObject;
    },

    throwError = (errorString, asType) => {
        let errorMessage = new Error(),

            stackTrace = (function() {
                let jsError = new Error(errorString);
                if(!!errorMessage.stack) {
                    let stackString = errorMessage.stack.toString().split('@').slice(2).join(' --> ');
                    return jsError.toString() +' --> '+ stackString;
                }
                return jsError;
            }()),

            types = {
                'console':function() {
                    !!global.console && console.error(stackTrace);
                },
                'exception':function() {
                    throw stackTrace;
                }
            };
        asType = (asType in types) ? asType : 'console';
        types[asType]();
    },

    tryCatch = (params) => {
        let defaults = {
            ['try']:function() {},
            ['catch']:function() {},
            ['finally']:function() {}
        },
            result;

        extend(params).with(defaults);

        try {
            result = params.try();
        }
        catch(err) {
            result = params.catch(err);
        }
        finally {
            result = params.finally();
        }

        return result;
    },

    typeOf = (target, type) => {
        let parsedType = Object.prototype.toString.call(target).split(' ')[1].replace(/[\[\]]/g, '').toLowerCase();

        return (type) ? parsedType === type : parsedType;
    },

    console = () => {
        if(!window.console) {
            window.console = {
                assert:function() {},
                count:function() {},
                dir:function() {},
                dirxml:function() {},
                error:function() {},
                group:function() {},
                groupCollapsed:function() {},
                groupEnd:function() {},
                info:function() {},
                log:function() {},
                profile:function() {},
                profileEnd:function() {},
                table:function() {},
                time:function() {},
                timeEnd:function() {},
                timeStamp:function() {},
                trace:function() {},
                warn:function() {}
            }
        }

        return window.console;
    },

    waitFor = (targetObject, timeout = 60000) => {
        let deferred = promise(),
            interval = 20,
            prettyTimeout = (timeout / 1000) +' seconds',

            wait = setInterval(function() {
                var response = targetObject();
                if(response != null && response !== false) {
                    clearInterval(wait);
                    deferred.resolve(response);
                    return response;
                }

                if(timeout === 0) {
                    clearInterval(wait);
                    deferred.reject('waitFor timed out after '+ prettyTimeout);
                }

                timeout -= interval;

            }, interval);

        return deferred;
    };

console();

export { extend, tryCatch, throwError, clone, typeOf, waitFor };