import typeOf from 'type-of';

const extend = (target = {}) => {
    return {
        'with':(extensionObject = {}, params = {}) => {

            params.deep = params.deep || false;
            params.clone = params.clone || false;

            let keys = Object.keys(extensionObject),
                responseObject = (params.clone) ? clone(target) : target;

            /**
             * I am using a for loop because I do not know how big the object I am extending is going to be,
             * and you can squeeze marginally more performance out of a for loop
             */
            for(let i = keys.length; i--;) {
                if(responseObject[keys[i]] === null || responseObject[keys[i]] === undefined) {
                    responseObject[keys[i]] = extensionObject[keys[i]];
                }
                else if(typeOf(responseObject[keys[i]], 'object') && typeOf(extensionObject[keys[i]], 'object') && params.deep) {
                    extend(responseObject[keys[i]]).with(extensionObject[keys[i]], {deep:true, clone:false});
                }
            }

            return responseObject;
        }
    };
};

export default extend;
